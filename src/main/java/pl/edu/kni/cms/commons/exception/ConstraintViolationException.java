package pl.edu.kni.cms.commons.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class ConstraintViolationException extends RuntimeException {

  public ConstraintViolationException() {
  }

  public ConstraintViolationException(String message) {
    super(message);
  }
}
