package pl.edu.kni.cms.security.data.repository;

import java.util.Optional;
import pl.edu.kni.cms.security.data.entity.User;

public interface UserRepository {

  Optional<User> findUserByUsernameOrEmail(String login);
  void save (User user);
}
