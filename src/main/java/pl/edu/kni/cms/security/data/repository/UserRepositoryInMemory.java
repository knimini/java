package pl.edu.kni.cms.security.data.repository;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import org.springframework.stereotype.Repository;
import pl.edu.kni.cms.commons.exception.ConstraintViolationException;
import pl.edu.kni.cms.security.data.entity.User;

@Repository
public final class UserRepositoryInMemory implements UserRepository {

  private final Set<User> users;

  public UserRepositoryInMemory() {
    users = new HashSet<>();
    users.add(new User(1L,"alex", "alex@kni.udu.pl", "supperPassword", true));
    users.add(new User(2L, "micheal", "micheal@kni.edu.pl", "kniIsAwesome", true));
  }

  @Override
  public Optional<User> findUserByUsernameOrEmail(String login) {
    return users.stream()
        .filter((user) -> isLoginMatch(user, login))
        .findFirst();
  }

  @Override
  public void save(User user) {
    boolean wasAdded = users.add(user);
    if (!wasAdded) {
      throw new ConstraintViolationException("Username already taken");
    }
  }

  private boolean isLoginMatch(User user, String login) {
    return login.equals(user.getUsername()) || login.equals(user.getEmail());
  }
}
