package pl.edu.kni.cms.security.data.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(of = "username")
@NoArgsConstructor
@AllArgsConstructor
public class User {

  private Long id;
  private String username;
  private String email;
  private String password;
  private Boolean enabled;



}
